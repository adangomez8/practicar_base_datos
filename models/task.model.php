<?php
//Interactúa con la tabla pagos
class TaskModel{

    //El modelo es el encargado de consultar a la base de datos
    function GetAllRegister() {

        // 1. abro la conexión con MySQL 
        $db = new PDO('mysql:host=localhost;'.'dbname=db_tp3;charset=utf8', 'root', '');
    
        // 2. enviamos la consulta (3 pasos)
        $sentencia = $db->prepare("SELECT * FROM pagos"); // prepara la consulta
        $sentencia->execute(); // ejecuta
        $pagos = $sentencia->fetchAll(PDO::FETCH_OBJ); // obtiene la respuesta
    
        return $pagos;
    }

}