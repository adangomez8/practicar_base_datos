<?php

/**
 * Se conecta a la base de datos, y trae todas las tareas.
 */
function cargarRegistros() {

    // 1. abro la conexión con MySQL 
    $db = new PDO('mysql:host=localhost;'.'dbname=db_tp3;charset=utf8', 'root', '');

    // 2. enviamos la consulta (3 pasos)
    $sentencia = $db->prepare("SELECT * FROM pagos"); // prepara la consulta
    $sentencia->execute(); // ejecuta
    $pagos = $sentencia->fetchAll(PDO::FETCH_OBJ); // obtiene la respuesta

    return $pagos;
}

/**
 * Inserta un registro en la base da datos
 */
function insertPay($deudor, $cuota, $cuota_capital, $fecha_pago) {
     // 1. abro la conexión con MySQL 
     $db = new PDO('mysql:host=localhost;'.'dbname=db_tp3;charset=utf8', 'root', '');

     // 2. enviamos la consulta
    $sentencia = $db->prepare("INSERT INTO pagos(deudor, cuota, cuota_capital, fecha_pago) VALUES(?, ?, ?, ?)"); // prepara la consulta
    $sentencia->execute([$deudor, $cuota, $cuota_capital, $fecha_pago]); // ejecuta

}

function deleteTask($id){
     // 1. abro la conexión con MySQL 
     $db = new PDO('mysql:host=localhost;'.'dbname=db_tp3;charset=utf8', 'root', '');

     // 2. enviamos la consulta (3 pasos)
     $sentencia = $db->prepare("DELETE FROM pagos WHERE id_pagos=?"); // prepara la consulta
     $sentencia->execute([$id]);
     
}

function UpdatepagarDeuda($id){
    //1. abro la conexión con MySQL 
   $db = new PDO('mysql:host=localhost;'.'dbname=db_tp3;charset=utf8', 'root', '');

     //2. enviamos la consulta (3 pasos)
    $sentencia = $db->prepare("UPDATE pagos set pagado = 1 WHERE id_pagos=?"); // prepara la consulta
    $sentencia->execute([$id]);
   }

function isRepeated($deudor, $cuota){
    // 1. abro la conexión con MySQL 
    $db = new PDO('mysql:host=localhost;'.'dbname=db_tp3;charset=utf8', 'root', '');

    // 2. enviamos la consulta (3 pasos)
    $sentencia = $db->prepare("SELECT * FROM pagos WHERE deudor = ". $deudor . " AND cuota = ". $cuota .""); // prepara la consulta
    $sentencia->execute(); // ejecuta
    $pagos = $sentencia->fetchAll(PDO::FETCH_OBJ); // obtiene la respuesta

    return empty($pagos);
}
